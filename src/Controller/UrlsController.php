<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Http\Client;
use Cake\Validation\Validation;
use App\Model\Table\UrlsTable;

class UrlsController extends AppController
{

	public function initialize()
    {
        parent::initialize();
		ini_set('max_execution_time',0);
    }
	
    public function index()
    {
		$urls = $this->Urls->find('all',[
			'conditions'=>[
				'parsed' => UrlsTable::PARSED
			],
			'order'=>['modified'=>'desc']
		]);
        $urls = $this->paginate($urls);
		
		$urlsUnparsed  = $this->Urls->find('all',[
			'conditions'=>[
				'parsed' => UrlsTable::NOT_PARSED
			],
			'order'=>['modified'=>'desc']
		])->count();

        $this->set(compact(['urls','urlsUnparsed']));
    }
	
    public function add()
    {
        $url = $this->Urls->newEntity();
        if ($this->request->is('post')) {
            $url = $this->Urls->patchEntity($url, $this->request->getData());
            if ($this->Urls->save($url)) {
                $this->Flash->success(__('The url has been saved.'));
				$this->processRecursiveUrls($url->url,$url->id);
            }
            $this->Flash->error(__('The url could not be saved. Please, try again.'));
        }
        $this->set(compact('url'));
    }
	
	private function processRecursiveUrls($url,$urlId){
		$http = new Client();;
		$response = $http->get($url);
		$urlData = [];
		if($response->isOk()){
			$body = $response->body();
			$doc = new \DOMDocument();
			@$doc->loadHTML($body);
			foreach($doc->getElementsByTagName('a') as $link) {
				$url = $link->getAttribute('href');
				if(Validation::url($url,true)){
					$urlData[] = rtrim($url,"/");
				}	
			}
			$this->saveFetchedUrls($urlData);
		}
		$this->Urls->updateParsedStatus($urlId);
		$unparsedUrl = $this->getAnUnparsedUrl();
		if(!empty($unparsedUrl)){
			$this->processRecursiveUrls($unparsedUrl->url,$unparsedUrl->id);
		} else {
			return $this->redirect(['action' => 'index']);
		}
		
	}
	
	private function saveFetchedUrls($urlData){
		if(!empty($urlData)){
			foreach($urlData as $key=>$url){
				$urlEntity = $this->Urls->newEntity(['url'=>$url]);
				if(empty($urlEntity->getErrors())){
					$this->Urls->save($urlEntity);
				} else {
					continue;
				}
			}
		}
	}
	
	private function getAnUnparsedUrl(){
		return $this->Urls->find('all',[
			'conditions' => [
				'parsed' => UrlsTable::NOT_PARSED
			]
		])->first();
	}
	
	public function dashboard(){
		
	}

	
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $url = $this->Urls->get($id);
        if ($this->Urls->delete($url)) {
            $this->Flash->success(__('The url has been deleted.'));
        } else {
            $this->Flash->error(__('The url could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
