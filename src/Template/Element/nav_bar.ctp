<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Urls'), ['action' => 'index']) ?></li>
		<li><?= $this->Html->link(__('New Url'), ['action' => 'add']) ?></li>
    </ul>
</nav>