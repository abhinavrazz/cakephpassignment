<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Url[]|\Cake\Collection\CollectionInterface $urls
 */
?>
<div class="urls index large-9 medium-8 columns content">
    <h3><?= __('Latest Urls Parsed') ?></h3>
    <h5><?= __('Total Urls Parsed: '). $this->request['paging']['Urls']['count'] ?> </h5>
    <h5><?= __('Total number of Queued URL\'s: '). $urlsUnparsed; ?> </h5>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('url') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($urls as $url): ?>
            <tr>
                <td><?= $this->Number->format($url->id) ?></td>
                <td><?= $url->url ?></td>
                <td><?= $this->Time->format($url->created,'dd MMM YYY HH:mm:ss') ?></td>
                <td><?= $this->Time->format($url->modified,'dd MMM YYY HH:mm:ss') ?></td>
                <td class="actions">
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $url->id], ['confirm' => __('Are you sure you want to delete # {0}?', $url->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
