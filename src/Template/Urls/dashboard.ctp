<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart', 'line']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Time', 'Per Minute'],
          ['2004',  1000],
          ['2005',  1170],
          ['2006',  660],
          ['2007',  1030]
        ]);

        var options = {
          title: 'Url Parsed Per Minutes',
          curveType: 'function',
          legend: { position: 'bottom' },
		  hAxis: {
			  title: 'Time'
			},
			vAxis: {
			  title: 'URL Parsed'
			}
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
    </script>
<div class="urls index large-9 medium-8 columns content">
   <div id="curve_chart" style="width: 900px; height: 500px"></div>
</div>
