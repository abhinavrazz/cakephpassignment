<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Url $url
 */
?>
<div class="urls form large-9 medium-8 columns content">
    <?= $this->Form->create($url,['novalidate'=>true]) ?>
    <fieldset>
        <legend><?= __('Add Url') ?></legend>
        <?php
            echo $this->Form->control('url');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
