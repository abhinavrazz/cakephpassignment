<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Urls Model
 *
 * @method \App\Model\Entity\Url get($primaryKey, $options = [])
 * @method \App\Model\Entity\Url newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Url[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Url|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Url|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Url patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Url[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Url findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UrlsTable extends Table
{

	const NOT_PARSED = 0 ;
	const PARSED = 1 ;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('urls');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
			->requirePresence('url', 'create')
            ->notEmpty('url')
			->add('url','validUrl',[
					'rule'=>['url',true],
					'message'=>"Please enter a valid url"
				]
			)
			->add('url','unique',[
					'rule' => 'validateUnique',
					'provider' => 'table',
					'message' => 'This URL has already been used.'
				]
			);

        return $validator;
    }
	
	public function updateParsedStatus($url_id){
		$this->query()
			->update()
			->set(['parsed'=> self::PARSED,'modified'=> date('Y-m-d H:i:s')])
			->where(['id'=>$url_id])
			->execute();
	}
}
